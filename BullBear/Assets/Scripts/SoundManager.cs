﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public struct AudioInstantiater
{
	public AudioClip clip;
	public bool loop;
	public bool playAwake;
	public float vol;
}

public class SoundManager : MonoBehaviour {
	public static SoundManager instance;
	Dictionary<string, AudioSource> theDic = new Dictionary<string, AudioSource>();

	public AudioInstantiater[] theClipsToPlay;
	// Use this for initialization
	void Start () {
		for (int i = 0; i < theClipsToPlay.Length; i++)
		{
			CreateAudio (theClipsToPlay[i]);
		}
		SoundManager.instance = this;
	}
	
	void CreateAudio(AudioClip clip, bool loop, bool playAwake, float vol)
	{
		AudioSource newSource = gameObject.AddComponent<AudioSource>();
		newSource.clip = clip;
		newSource.loop = loop;
		newSource.playOnAwake = playAwake;
		newSource.volume = vol;
		theDic.Add (clip.name, newSource);
	}

	void CreateAudio(AudioInstantiater theaudio)
	{
		AudioSource newSource = gameObject.AddComponent<AudioSource>();
		newSource.clip = theaudio.clip;
		newSource.loop = theaudio.loop;
		newSource.playOnAwake = theaudio.playAwake;
		newSource.volume = theaudio.vol;
		theDic.Add (theaudio.clip.name, newSource);
	}

	public static void PlayMusic(string audioname)
	{
		SoundManager.instance.PlayMusic_f (audioname);
	}

	void PlayMusic_f(string audioname)
	{
		if (theDic.ContainsKey (audioname))
		{
			theDic [audioname].Play ();
		}
	}
}
