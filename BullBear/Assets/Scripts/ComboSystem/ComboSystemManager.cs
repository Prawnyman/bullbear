﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

public enum Combos_e
{
	Dash = 0
}
public class ComboSystemManager 
{
	public ComboSystemManager()
	{
	}

	public static void FindCombination( List<Directions_Enum> InputCombo)
	{
		Dash.CheckDash (InputCombo);
	}
}

public class Dash : MonoBehaviour
{
	public delegate void ComboAction (Combos_e theCombo, Directions_Enum direction);
	public static event ComboAction OnCombo;

	static List<Directions_Enum> leftDash = new List<Directions_Enum>(){Directions_Enum.Left, Directions_Enum.Left};
	static List<Directions_Enum> rightDash = new List<Directions_Enum>(){Directions_Enum.Right, Directions_Enum.Right};

	public static void CheckDash(List<Directions_Enum> InputCombo )
	{
		if (InputCombo.Count == 2)
		{
			if (InputCombo [0] == rightDash [0] && InputCombo [1] == rightDash [1])
			{
				OnCombo (Combos_e.Dash, Directions_Enum.Right);
			}
			else if(InputCombo [0] == leftDash [0] && InputCombo [1] == leftDash [1])
			{
				OnCombo (Combos_e.Dash, Directions_Enum.Left);
			}
		}
	}

}

