﻿using UnityEngine;
using System.Collections;

public class ScreenShake : MonoBehaviour {

	Camera camera; // set this via inspector
	public float shake = 0;
	float shakeAmount = 0.7f;
	float decreaseFactor  = 1.0f;
	public GameObject canvas;

	void Update() {
		if (shake > 0) {
			canvas.transform.localPosition = Random.insideUnitSphere * shakeAmount;
			shake -= Time.deltaTime * decreaseFactor;

		} else {
			shake = 0.0f;
		}
	}
}
