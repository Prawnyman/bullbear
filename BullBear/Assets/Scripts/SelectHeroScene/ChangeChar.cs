﻿using UnityEngine;
using System.Collections;
using InControl;
using UnityEngine.SceneManagement;

public class ChangeChar : MonoBehaviour {
	GameObject [] Bears;
	GameObject [] Bulls;

	public Transform BearsParent;
	public Transform BullsParent;

	public GetChar getChar;
	// Use this for initialization
	void Start () {
		Bears = new GameObject[BearsParent.childCount];
		for (int i = 0; i < BearsParent.childCount; i++)
		{
			Bears [i] = BearsParent.GetChild (i).gameObject;
		}
		Bulls = new GameObject[BullsParent.childCount];
		for (int i = 0; i < BullsParent.childCount; i++)
		{
			Bulls [i] = BullsParent.GetChild (i).gameObject;
		}
		player1 = PlayerActions.CreateWithDefaultBindings (1);
		player2 = PlayerActions.CreateWithDefaultBindings (2);

		if (0 < InputManager.Devices.Count)
		{
			player1.Device = InputManager.Devices [0];
		}
		if (1 < InputManager.Devices.Count)
		{
			player2.Device = InputManager.Devices [1];
		}
		StartCoroutine (WaitForNextStage ());
	}

	IEnumerator WaitForNextStage()
	{
		while (!selected2 || !selected)
		{
			yield return null;
		}
		string scenetoLoad = "Level ";
		scenetoLoad = scenetoLoad + Random.Range ((int)1, (int)2).ToString();
		SceneManager.LoadScene(scenetoLoad);
	}
	PlayerActions player1;
	PlayerActions player2;

	public GameObject Selector1;
	public GameObject Selector2;

	int selectcount1 = 0;
	int selectcount2 = 0;

	bool selected = false;
	bool selected2 = false;

	public GameObject Selected;
	public GameObject Selected2;

	// Update is called once per frame
	void Update () {
		if (!selected)
		{
			if (player1.Left.IsPressed && player1.Left.LastState != player1.Left.IsPressed)
			{
				selectcount1--;
				selectcount1 = Mathf.Clamp (selectcount1, 0, 7);
				Selector1.transform.position = Bulls [selectcount1].transform.position;
			} else if (player1.Right.IsPressed && player1.Right.LastState != player1.Right.IsPressed)
			{
				selectcount1++;
				selectcount1 = Mathf.Clamp (selectcount1, 0, 7);
				Selector1.transform.position = Bulls [selectcount1].transform.position;
			}
		}

		if (!selected2)
		{
			if (player2.Left.IsPressed && player2.Left.LastState != player2.Left.IsPressed)
			{
				selectcount2--;
				selectcount2 = Mathf.Clamp (selectcount2, 0, 7);
				Selector2.transform.position = Bears [selectcount2].transform.position;
			} else if (player2.Right.IsPressed && player2.Right.LastState != player2.Right.IsPressed)
			{
				selectcount2++;
				selectcount2 = Mathf.Clamp (selectcount2, 0, 7);
				Selector2.transform.position = Bears [selectcount2].transform.position;
			}
		}

		if (player1.Dash.IsPressed && player1.Dash.LastState != player1.Dash.IsPressed && !selected && (selectcount1 == 0 || selectcount1 == 1))
		{
			selected = true;
			Selected.SetActive (true);
			Selected.transform.position = Selector1.transform.position;
			Camera.main.GetComponent<ScreenShake> ().shake = 0.5f;
			getChar.char1 = selectcount1;

		}
		if (player2.Dash.IsPressed && player2.Dash.LastState != player2.Dash.IsPressed && !selected2 && (selectcount2 == 0 || selectcount2 == 1))
		{
			selected2 = true;
			Selected2.SetActive (true);
			Selected2.transform.position = Selector2.transform.position;
			Camera.main.GetComponent<ScreenShake> ().shake = 0.5f;
			getChar.char2 = selectcount2;
		}
	}
}
