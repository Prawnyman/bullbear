﻿using UnityEngine;
using System.Collections;
using InControl;
public class PlayerActions : PlayerActionSet {

	public PlayerAction Dash;
	public PlayerAction Special;
	public PlayerAction Jump;
	public PlayerAction Left;
	public PlayerAction Right;
	public PlayerAction Up;
	public PlayerAction Down;
	public PlayerOneAxisAction Move;

	// Use this for initialization
	public PlayerActions () {
		Dash = CreatePlayerAction( "Dash" );
		Special = CreatePlayerAction( "Special" );
		Jump = CreatePlayerAction( "Jump" );
		Left = CreatePlayerAction( "Move Left" );
		Right = CreatePlayerAction( "Move Right" );
		//Up = CreatePlayerAction( "Move Up" );
		Down = CreatePlayerAction( "Move Down" );
		Move = CreateOneAxisPlayerAction( Left, Right );

	}
	
	public static PlayerActions CreateWithDefaultBindings(int player)
	{
		var playerActions = new PlayerActions();

		if (player == 1)
		{
			playerActions.Dash.AddDefaultBinding ( Key.F);
			playerActions.Dash.AddDefaultBinding (InputControlType.Action3);

			playerActions.Special.AddDefaultBinding (Key.G);
			playerActions.Special.AddDefaultBinding (InputControlType.Action2);

			playerActions.Jump.AddDefaultBinding (Key.LeftAlt);
			playerActions.Jump.AddDefaultBinding (Key.W);
			playerActions.Jump.AddDefaultBinding (InputControlType.Action1);

			playerActions.Down.AddDefaultBinding (Key.S);
			playerActions.Left.AddDefaultBinding (Key.A);
			playerActions.Right.AddDefaultBinding (Key.D);

			//playerActions.Left.AddDefaultBinding 
			playerActions.Left.AddDefaultBinding (InputControlType.LeftStickLeft);
			playerActions.Right.AddDefaultBinding (InputControlType.LeftStickRight);
			playerActions.Down.AddDefaultBinding (InputControlType.LeftStickDown);

			playerActions.Left.AddDefaultBinding (InputControlType.DPadLeft);
			playerActions.Right.AddDefaultBinding (InputControlType.DPadRight);
			playerActions.Down.AddDefaultBinding (InputControlType.DPadDown);

			playerActions.ListenOptions.IncludeUnknownControllers = true;
			playerActions.ListenOptions.MaxAllowedBindings = 4;
			//			playerActions.ListenOptions.MaxAllowedBindingsPerType = 1;
			//			playerActions.ListenOptions.UnsetDuplicateBindingsOnSet = true;
			//			playerActions.ListenOptions.IncludeMouseButtons = true;

		} 
		else
		{
			playerActions.Dash.AddDefaultBinding (Key.J);
			playerActions.Dash.AddDefaultBinding (InputControlType.Action3);

			playerActions.Special.AddDefaultBinding (Key.K);
			playerActions.Special.AddDefaultBinding (InputControlType.Action2);

			playerActions.Jump.AddDefaultBinding (Key.RightAlt);
			playerActions.Jump.AddDefaultBinding (InputControlType.Action1);
			playerActions.Jump.AddDefaultBinding (Key.UpArrow);

			playerActions.Down.AddDefaultBinding (Key.DownArrow);
			playerActions.Left.AddDefaultBinding (Key.LeftArrow);
			playerActions.Right.AddDefaultBinding (Key.RightArrow);

			playerActions.Left.AddDefaultBinding (InputControlType.LeftStickLeft);
			playerActions.Right.AddDefaultBinding (InputControlType.LeftStickRight);
			playerActions.Down.AddDefaultBinding (InputControlType.LeftStickDown);

			playerActions.Left.AddDefaultBinding (InputControlType.DPadLeft);
			playerActions.Right.AddDefaultBinding (InputControlType.DPadRight);
			playerActions.Down.AddDefaultBinding (InputControlType.DPadDown);

			playerActions.ListenOptions.IncludeUnknownControllers = true;
			playerActions.ListenOptions.MaxAllowedBindings = 4;
		}
		return playerActions;
	}
}
