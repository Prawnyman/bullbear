﻿using UnityEngine;
using System.Collections;
using InControl;

public class InputControl : MonoBehaviour {

	public ChangeSprite theSpriteChanger;
	public bool SelectHero = false;
	// Update is called once per frame
	void Update () {
		InputDevice device = InputManager.ActiveDevice;

		if (device.LeftStick.Left.IsPressed &&  device.LeftStick.Left.LastState !=  device.LeftStick.Left.IsPressed|| Input.GetKeyUp (KeyCode.LeftArrow) || Input.GetKeyUp (KeyCode.A))
		{
			if (!SelectHero)
			{
				theSpriteChanger.OnReceiveEvent (Left_Right.Left);
			} else
			{

			}
		} else if (device.LeftStick.Right.IsPressed &&  device.LeftStick.Right.LastState !=  device.LeftStick.Right.IsPressed || Input.GetKeyUp (KeyCode.RightArrow) || Input.GetKeyUp (KeyCode.D))
		{	if (!SelectHero)
			{
				theSpriteChanger.OnReceiveEvent (Left_Right.Right);
			}
		} else if (Input.GetKeyUp (KeyCode.Space) || device.Action1.IsPressed)
		{
			if (!SelectHero)
			{
				theSpriteChanger.OnEnter ();
			}
		} else if (Input.GetKeyUp (KeyCode.Escape) || device.Action2.WasPressed)
		{
			if (!SelectHero)
			{
				theSpriteChanger.Esc ();
			}
		}
	}
}
