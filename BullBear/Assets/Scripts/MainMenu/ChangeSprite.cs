﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public enum Left_Right
{
	Left = 0,
	Right
}
public class ChangeSprite : MonoBehaviour {
	
	public Sprite [] Sprites;
	int CurrentSprite = 0;
	public Image theImage; 

	public GameObject Credits;
	public GameObject Frame;

	public GameObject SelectHero;

	void Start()
	{
		theImage.sprite = Sprites [0];
	}

	public void OnReceiveEvent(Left_Right theDirection)
	{
		switch (theDirection)
		{
			case Left_Right.Left:
				CurrentSprite--;
				if (CurrentSprite < 0)
				{
					CurrentSprite = Sprites.Length - 1;
				}
				break;
			case Left_Right.Right:
				CurrentSprite++;
				if (CurrentSprite >= Sprites.Length)
				{
					CurrentSprite = 0;
				}
				break;
		}
		theImage.sprite = Sprites [CurrentSprite];
	}

	bool creditscene = false;

	bool Started = false;
	bool Started2 = false;

	IEnumerator SelectHero_e()
	{
		SelectHero.SetActive (true);
		while (!Started || !Started2)
		{
			yield return null;
		}
		string sceneToLoad = "Level ";sceneToLoad = "Level 1";
		//sceneToLoad = sceneToLoad + Random.Range ((int)0, (int)1).ToString();
		SceneManager.LoadScene(sceneToLoad);
	}

	public void OnEnter()
	{
		switch (CurrentSprite)
		{
			case 0:
				//Start games
				//StartCoroutine(SelectHero_e());
				SceneManager.LoadScene("Select");
				break;
			case 1:
				//intructions
				SceneManager.LoadScene("Training");
				break;
			case 2:
				//credits
				Credits.SetActive (true);
				Frame.SetActive (false);
				this.gameObject.SetActive (false);
				creditscene = true;
				break;
			default:
				Debug.LogWarning ("UNKNOWN !!!");
				break;
		}
	}

	public void Esc()
	{
		if (creditscene)
		{
			creditscene = false;
			Credits.SetActive (false);
			Frame.SetActive (true);
			this.gameObject.SetActive (true);
		}
	}
}
