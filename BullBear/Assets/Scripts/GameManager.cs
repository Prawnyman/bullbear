﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public GameObject player1;
	public GameObject player2;

	public int p1Lives;
	public int p2Lives;

	public Texture2D p1Texture;
	public Texture2D p2Texture;

	public GameObject win1;
	public GameObject win2;

	public GameObject getchar;

	public Transform pol;
	public Transform griz;
	public Transform bull;
	public Transform red;

	void OnGUI()
	{
		for (int i = 0; i < p1Lives; ++i)
		{
			GUI.DrawTexture (new Rect (20 + 50 * i, 20, 60, 60), p1Texture, ScaleMode.StretchToFill);
		}
		for (int i = 0; i < p2Lives; ++i)
		{
			GUI.DrawTexture (new Rect ((Screen.width - 20 - 60) - 50 * i, 20, 60, 60), p2Texture, ScaleMode.StretchToFill);
		}
	}

	// Use this for initialization
	void Start () {
		

			
		getchar = GameObject.Find ("GetChar");

		if (getchar.GetComponent<GetChar> ().char1 == 0)
			Instantiate (bull, new Vector2 (-8, 5), Quaternion.identity);
		else
			Instantiate (red, new Vector2 (-8, 5), Quaternion.identity);

		if (getchar.GetComponent<GetChar> ().char2 == 0)
			Instantiate (griz, new Vector2 (8, 5), Quaternion.identity);
		else
			Instantiate (pol, new Vector2 (8, 5), Quaternion.identity);

		GameObject[] players = GameObject.FindGameObjectsWithTag ("PC");

		foreach (GameObject go in players) {
			if (go.GetComponent<PlayerInfo> ().playerNumber == 1)
				player1 = go;
			else if (go.GetComponent<PlayerInfo> ().playerNumber == 2)
				player2 = go;
		}

		p1Lives = 5;
		p2Lives = 5;
	}
	
	// Update is called once per frame
	void Update () {
		if (p1Lives <= 0 || p2Lives <= 0) {
			GameEnd ();
		}
	}

	void GameEnd(){
		Physics2D.gravity = Vector2.zero;

		player1.GetComponent<ControlController> ().rb2d.velocity = Vector2.zero;
		player1.GetComponent<DeathManager> ().alive = false;
		player2.GetComponent<ControlController> ().rb2d.velocity = Vector2.zero;
		player2.GetComponent<DeathManager> ().alive = false;

		if (p1Lives <= 0)
		{
			win2.SetActive (true);
		} else
		{
			win1.SetActive (true);
		}
		StartCoroutine (WaitAndGoToMainMenu ());


		/*
		GameObject[] Everything = UnityEngine.Object.FindObjectsOfType<GameObject> ();
		foreach (GameObject go in Everything)
		{
			if (go.activeInHierarchy) {
				go.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
				go.GetComponent<Rigidbody2D> ().angularVelocity = 0;
			}
		}
		*/
	}

	IEnumerator WaitAndGoToMainMenu()
	{
		yield return new WaitForSeconds (3f);
		SceneManager.LoadScene ("Main Menu");
	}
}
