﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Background : MonoBehaviour
{
	const float AmountOfPoints = 1000;
	List<float> NoiseHolder = new List<float> ();
	List<float> NoiseHolder2 = new List<float> ();
	List<float> NoiseHolder3 = new List<float> ();

	public int MovementStep = 2;

	static Material lineMaterial;

	static void CreateLineMaterial ()
	{
		if (!lineMaterial)
		{
			// Unity has a built-in shader that is useful for drawing
			// simple colored things.
			var shader = Shader.Find ("Hidden/Internal-Colored");
			lineMaterial = new Material (shader);
			lineMaterial.hideFlags = HideFlags.HideAndDontSave;
			// Set blend mode to invert destination colors.
			lineMaterial.SetInt ("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusDstColor);
			lineMaterial.SetInt ("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
			// Turn off backface culling, depth writes, depth test.
			lineMaterial.SetInt ("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
			lineMaterial.SetInt ("_ZWrite", 1);
			lineMaterial.SetInt ("_ZTest", (int)UnityEngine.Rendering.CompareFunction.Always);
			// Turn off depth writes
			//lineMaterial.SetInt ("_ZWrite", 1);
		}
	}

	IEnumerator UpdateGraph ()
	{
		while (true)
		{
			//yield return new WaitForSeconds;
			//move the lines
			for (int i = 0; i < 50; i++)
			{
				NoiseHolder.Insert (0, NoiseHolder [NoiseHolder.Count - 1]);
				NoiseHolder.RemoveAt (NoiseHolder.Count - 1);

				NoiseHolder2.Insert (0, NoiseHolder [NoiseHolder.Count - 1]);
				NoiseHolder2.RemoveAt (NoiseHolder.Count - 1);

				NoiseHolder3.Insert (0, NoiseHolder [NoiseHolder.Count - 1]);
				NoiseHolder3.RemoveAt (NoiseHolder.Count - 1);
			}
		}
	}

	void Awake ()
	{
		List<float> tempAdder = new List<float> ();
		for (int i = 0; i < AmountOfPoints; i++)
		{
			tempAdder.Add (i / AmountOfPoints);
		}
		for (float i = 0.00f; i < 1; i += 1 / AmountOfPoints)
		{
			NoiseHolder.Add (Mathf.PerlinNoise (i * 50 + 0.01f, 0f));
			NoiseHolder2.Add (Mathf.PerlinNoise (i * 40 - 0.05f, 0.4f));
			NoiseHolder3.Add (Mathf.PerlinNoise (i * 60 - 0.05f, 0.8f));
		}

		for (int i = 0; i < AmountOfPoints; i++)
		{
			//NoiseHolder [i] += tempAdder [i];
		}
		CreateLineMaterial ();
		//StartCoroutine (UpdateGraph ());
	}

	public int zline = 0;

	public void OnPostRender ()
	{
		//render the lines
		// Set your materials

		GL.PushMatrix ();
		//GL.LoadPixelMatrix ();
		GL.LoadOrtho ();
		lineMaterial.SetPass (0);
		GL.Begin (GL.LINES);
		GL.Color (new Color (0, 1, 0));
		float x = 0.00f;
		for (int i = 0; i < AmountOfPoints; i++, x += (1 / AmountOfPoints))
		{
			//GL.Begin (GL.LINES);
			GL.Vertex3 (x, NoiseHolder [i], zline);
			if (i + 1 >= NoiseHolder.Count)
			{
				GL.Vertex3 (x + (1 / AmountOfPoints), NoiseHolder [i], zline);
			} else
			{
				GL.Vertex3 (x + (1 / AmountOfPoints), NoiseHolder [i + 1], zline);
			}
		}
		GL.End ();
		GL.Begin (GL.LINES);
		GL.Color (new Color (1, 0, 0));
		x = 0.00f;
		for (int i = 0; i < AmountOfPoints; i++, x += (1 / AmountOfPoints))
		{
			//GL.Begin (GL.LINES);
			GL.Vertex3 (x, NoiseHolder2 [i] + 0.1f, zline);
			if (i + 1 >= NoiseHolder2.Count)
			{
				GL.Vertex3 (x + (1 / AmountOfPoints), NoiseHolder2 [i] + 0.1f, zline);
			} else
			{
				GL.Vertex3 (x + (1 / AmountOfPoints), NoiseHolder2 [i + 1] + 0.1f, zline);
			}
		}
		GL.End ();
		GL.Begin (GL.LINES);
		GL.Color (new Color (0, 0, 1));
		x = 0.00f;
		for (int i = 0; i < AmountOfPoints; i++, x += (1 / AmountOfPoints))
		{
			//GL.Begin (GL.LINES);
			GL.Vertex3 (x, NoiseHolder3 [i] - 0.1f, zline);
			if (i + 1 >= NoiseHolder3.Count)
			{
				GL.Vertex3 (x + (1 / AmountOfPoints), NoiseHolder3 [i] - 0.1f, zline);
			} else
			{
				GL.Vertex3 (x + (1 / AmountOfPoints), NoiseHolder3 [i + 1] - 0.1f, zline);
			}
		}
		GL.End ();

		// Draw your stuff
		GL.PopMatrix ();

		for (int i = 0; i < MovementStep; i++)
		{
			NoiseHolder.Insert (0, NoiseHolder [NoiseHolder.Count - 1]);
			NoiseHolder.RemoveAt (NoiseHolder.Count - 1);

			NoiseHolder2.Insert (0, NoiseHolder [NoiseHolder.Count - 1]);
			NoiseHolder2.RemoveAt (NoiseHolder.Count - 1);

			NoiseHolder3.Insert (0, NoiseHolder [NoiseHolder.Count - 1]);
			NoiseHolder3.RemoveAt (NoiseHolder.Count - 1);
		}
	}
}
