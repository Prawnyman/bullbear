﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Directions_Enum
{
	Up = 0,
	Down,
	Left,
	Right
}

public enum Player_State
{
	IDLE = 0,
	WALKING,
	CROUCHING,
	ROLLING,
	JUMPING,
	FALLING,
	DASHING,
	HURT,
	STUN,
	SPECIAL 
}

public class PlatfromControl : MonoBehaviour {
	[HideInInspector] public bool facingRight = false;
	[HideInInspector] public bool jump = false;
	[HideInInspector] public bool candoublejump = false;
	public float moveForce = 365f;
	public float maxSpeed = 5f;
	public float jumpForce = 1000f;
	private Transform groundCheck;	//object that is checked if overlapping with ground

	public int jumpCount;
	public int jumpLimit;

	//Inputs
	public string horizontalInput;
	public string verticalInput;
	public string jumpInput;
	public string dashInput;
	public string specialInput;

	//Movement stats
	public float DashPower = 50;
	public float SmashPower = 100;
	public float RollPower = 50;

	public bool canDash = true;
	public float dashCooldown = 0.5f;

	//States
	public bool grounded = false;
	public bool Crouching = false;
	public bool Dashing = false;
	public bool Rolling = false;
	public bool KnockedBack = false;

	public float MaxKnockoutMag = 90f;
	public float KnockoutTime = 0.2f;

	private Animator anim;
	private Rigidbody2D rb2d;

	public GameObject opponent;

	// Use this for initialization
	void Awake () 
	{
		jumpCount = 0;
		jumpLimit = 2;

		if (this.GetComponent<PlayerInfo> ().playerNumber == 2)
			Flip ();

		horizontalInput = "Horizontal" + this.GetComponent<PlayerInfo>().playerNumber; 
		verticalInput = "Vertical" + this.GetComponent<PlayerInfo>().playerNumber; 
		jumpInput = "Jump" + this.GetComponent<PlayerInfo>().playerNumber.ToString();
		dashInput = "Dash" + this.GetComponent<PlayerInfo>().playerNumber.ToString();
		specialInput = "Special" + this.GetComponent<PlayerInfo>().playerNumber.ToString();


		groundCheck = transform.Find("groundCheck");
		rb2d = GetComponent<Rigidbody2D>();
		Dash.OnCombo += OnComboEvent;

	}

	void Start()
	{
		anim = GetComponentInChildren<Animator> ();

		GameObject[] temp = GameObject.FindGameObjectsWithTag ("Player");
		for (int i = 0; i < 2; i++) {
			if (temp [i].gameObject != this.gameObject) {
				opponent = temp [i];
				break;
			}
		}
	}
	
	IEnumerator Dash_e(Vector2 force, float time)
	{
		Dashing = true;
		canDash = false;

		StartCoroutine (DashCooldown_e (dashCooldown));

		rb2d.velocity = force;

		yield return new WaitForSeconds (time);
		rb2d.velocity = Vector2.zero;
		Dashing = false;
	}

	IEnumerator Roll_e(Vector2 force, float time)
	{
		Rolling = true;
		rb2d.velocity = force;

		yield return new WaitForSeconds (time);
		rb2d.velocity = Vector2.zero;
		Rolling = false;
	}

	public IEnumerator KnockBackOpponent_e(Vector2 force, float time)
	{
		opponent.GetComponent<PlatfromControl>().KnockedBack = true;

		yield return new WaitForSeconds (time);
		opponent.GetComponent<PlatfromControl>().KnockedBack = false;
	}

	IEnumerator DashCooldown_e(float time)
	{
		yield return new WaitForSeconds (time);
		canDash = true;
	}


	void OnComboEvent(Combos_e theCombo, Directions_Enum theDir)
	{
		if(theCombo == Combos_e.Dash)
		{
			if (theDir == Directions_Enum.Left)
			{
				StartCoroutine (Dash_e (new Vector2 (-DashPower, 0), 0.05f));
			} else if (theDir == Directions_Enum.Right)
			{
				StartCoroutine (Dash_e(new Vector2 (DashPower, 0), 0.05f));
			}
		}
	}


	// Update is called once per frame
	void Update () 
	{
		grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

		if (grounded && jumpCount > 0) {
			jumpCount = 0;
		}

		if (Input.GetButtonDown(dashInput))
		{
			if (Input.GetAxis (verticalInput) < 0 && grounded) {
				//Roll code
				Debug.Log ("Roll");
				if (facingRight) {
					StartCoroutine (Roll_e (new Vector2 (RollPower, 0), 0.5f));
				}
				else {
					StartCoroutine (Roll_e (new Vector2 (-RollPower, 0), 0.5f));
				}
			}
			else if (Input.GetAxis (verticalInput) < 0 && !grounded) {
				StartCoroutine (Dash_e (new Vector2 (0, -SmashPower), 0.05f));
			} 
			else {
				if (canDash == true) {
					if (facingRight)
						StartCoroutine (Dash_e (new Vector2 (DashPower, 0), 0.1f));
					else
						StartCoroutine (Dash_e (new Vector2 (-DashPower, 0), 0.1f));
				}
			}
		}



		if (Input.GetButtonDown(jumpInput) && jumpCount < jumpLimit) {
			jump = true;
			jumpCount++;
		}


	}


	void FixedUpdate()
	{
		if (Rolling) {
			if (opponent.GetComponent<PlatfromControl> ().Crouching == true) { //If crouching
				Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer ("Player"), LayerMask.NameToLayer ("Player"), false); //block
				Debug.Log ("ROLLING BLOCK");
			} else {
				Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer ("Player"), LayerMask.NameToLayer ("Player"), true); //pass through
				GetComponentInChildren<Renderer>().sortingOrder = opponent.GetComponentInChildren<Renderer>().sortingOrder - 1; //Layer sprite behind opponent
				Debug.Log ("ROLLING BY");
			}
		} else {
			if (opponent.GetComponent<PlatfromControl> ().Rolling == false) { //If crouching
				Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer ("Player"), LayerMask.NameToLayer ("Player"), false); //block
			}
		}

		if (KnockedBack)
		{
			rb2d.velocity = Vector2.ClampMagnitude (rb2d.velocity, MaxKnockoutMag);
		}
		else if (!Dashing && !Rolling)
		{
			float h = Input.GetAxis (horizontalInput);

			if (h * rb2d.velocity.x < maxSpeed)
				rb2d.AddForce (Vector2.right * h * moveForce);

			if (Mathf.Abs (rb2d.velocity.x) > maxSpeed)
				rb2d.velocity = new Vector2 (Mathf.Sign (rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);

			if (h > 0 && !facingRight)
				Flip ();
			else if (h < 0 && facingRight)
				Flip ();
		
			if (jump)
			{
				//anim.SetTrigger("Jump");
				rb2d.velocity = Vector2.zero;
				rb2d.angularVelocity = 0;
				rb2d.AddForce (new Vector2 (0f, jumpForce - (jumpCount * 1000f)));
				jump = false;
			}
		}
		if (grounded && (Input.GetAxis (verticalInput) < 0)) {
			anim.SetBool ("crouching", true);
			Crouching = true;
		}
		else if (grounded && Input.GetAxis (verticalInput) == 0 || Input.GetAxis(horizontalInput) > 0 || Input.GetAxis(horizontalInput) < 0)
		{
			anim.SetBool("crouching", false);
			Crouching = false;
		}
	}


	void Flip()
	{
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	void OnCollisionEnter2D (Collision2D col)
	{
		if(col.gameObject.layer == LayerMask.NameToLayer ("Player"))
		{
			if (Dashing) {
				//Debug.Log ("me" + rb2d.velocity);
				//Debug.Log (col.gameObject.GetComponent<PlatfromControl> ().rb2d.velocity);

				if (col.gameObject.GetComponent<PlatfromControl> ().Dashing == true) { //If both dashing
					//Both hit

				} 
				else 
				{
					//Hit them only
					col.gameObject.GetComponent<PlatfromControl>().StartCoroutine(KnockBackOpponent_e(col.gameObject.GetComponent<PlatfromControl> ().rb2d.velocity * 2, 0.2f));
				}
				//Debug.Log (col.gameObject.GetComponent<PlatfromControl> ().rb2d.velocity);

				Dashing = false;
				//Debug.Log ("hit");

			}
		}
	}

}
