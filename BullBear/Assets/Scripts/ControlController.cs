﻿using UnityEngine;
using System.Collections;
using InControl;

public class ControlController : MonoBehaviour {

	public float maxSpeed = 5f;
	public float MoveForce = 365f;
	public float jumpForce = 1000f;

	public int jumpCount = 0;
	public int jumpLimit = 2;

	public bool canDash = true;
	public bool canSpecial = true;
	public float dashCooldown = 0.5f;
	public float specialCooldown = 1f;

	public float MaxKnockoutMag = 300f;
	public float KnockoutTime = 0.2f;

	//Movement stats
	public float DashPower = 50f;
	public float SmashPower = 100f;
	public float RollPower = 10f;

	static int DeviceCount = 0;

	PlayerActions playerActions;
	PlayerInfo playerInfo;
	public Rigidbody2D rb2d;
	Animator anim;
	Transform groundCheck;

	bool facingRight = true;

	public bool grounded = false;
	public bool Crouching = false;
	public bool Dashing = false;
	public bool Slamming = false;
	public bool Rolling = false;
	public bool KnockedBack = false;
	public bool SpecialAttacking = false;

	public GameObject opponent;

	public Transform HitShockwave;

	void Start()
	{
		if (this.GetComponent<PlayerInfo> ().playerNumber == 2)
			Flip ();

		groundCheck = transform.Find("groundCheck");

		playerInfo = this.GetComponent<PlayerInfo>();

		playerActions = PlayerActions.CreateWithDefaultBindings(playerInfo.playerNumber);

		rb2d = this.GetComponent<Rigidbody2D> ();

		anim = GetComponentInChildren<Animator>();

		InputManager.OnDeviceAttached += AttachDevice;
		if (DeviceCount < InputManager.Devices.Count)
		{
			playerActions.Device = InputManager.Devices [DeviceCount];
			DeviceCount++;
		} 


		Debug.Log ("Devices count is " + InputManager.Devices.Count);

		//opponent = GameObject.FindGameObjectWithTag ("PC");

		GameObject[] temp = GameObject.FindGameObjectsWithTag ("PC");
		for (int i = 0; i < 2; i++) {
			if (temp [i].gameObject != this.gameObject) {
				opponent = temp [i];
				break;
			}
		}
	}

	void AttachDevice(InputDevice device)
	{
		Debug.Log ("Device attached");
	} 

	void Update()
	{
		grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

		if (!grounded && rb2d.velocity.y < 0) {
			if (anim.GetBool ("special") == true && GetComponentInChildren<PlayerInfo> ().playerName == "Redbull") {
				anim.SetBool ("special", false);
				Dashing = false;
			}
			anim.SetBool ("falling", true);
		} else {
			anim.SetBool ("falling", false);
		}

		if (Rolling) {
			anim.SetBool ("rolling", true);
			if (opponent.GetComponent<ControlController> ().Crouching == true) { //If crouching
				Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer ("Player"), LayerMask.NameToLayer ("Player"), false); //block
				//Debug.Log ("ROLLING BLOCK");
			} else {
				Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer ("Player"), LayerMask.NameToLayer ("Player"), true); //pass through
				GetComponentInChildren<Renderer>().sortingOrder = opponent.GetComponentInChildren<Renderer>().sortingOrder - 1; //Layer sprite behind opponent
				//Debug.Log ("ROLLING BY");
			}
		} else {
			if (opponent.GetComponent<ControlController> ().Rolling == false) { //If crouching
				Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer ("Player"), LayerMask.NameToLayer ("Player"), false); //block
			}
		}

		if (KnockedBack)
		{
			rb2d.velocity = Vector2.ClampMagnitude (rb2d.velocity, MaxKnockoutMag);
			anim.SetBool ("knockout", true);
		}
		else if (!Dashing && !Rolling && !KnockedBack)
		{
			anim.SetBool ("knockout", false);

			if (grounded && jumpCount > 0) 
				jumpCount = 0;

			if (grounded && (Mathf.Abs(playerActions.Move) > 0))
			{
				anim.SetBool ("walking", true);
			}
			else
			{
				anim.SetBool ("walking", false);
			}

			if (!Crouching) {
				if (playerActions.Move * rb2d.velocity.x < maxSpeed)
					rb2d.AddForce (Vector2.right * playerActions.Move * MoveForce);

				if (Mathf.Abs (rb2d.velocity.x) > maxSpeed)
					rb2d.velocity = new Vector2 (Mathf.Sign (rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);
			}

			if (playerActions.Move > 0 && !facingRight)
				Flip ();
			else if (playerActions.Move < 0 && facingRight)
				Flip ();
		
			if (playerActions.Dash.IsPressed && playerActions.Dash.LastState != playerActions.Dash.IsPressed)
			if (playerActions.Dash)
			{
				if (Crouching) {
					//Roll
					if (facingRight) {
						StartCoroutine (Roll_e (new Vector2 (RollPower, 0), 0.5f));
					} else {
						StartCoroutine (Roll_e (new Vector2 (-RollPower, 0), 0.5f));
					}
				} else if (playerActions.Down.IsPressed && !grounded) {
					//Smash down
					StartCoroutine (Dash_e (new Vector2 (0, -SmashPower), 0.2f));
					anim.SetBool ("slamming", true);
				} else {
					if (canDash == true) {
						if (facingRight)
							StartCoroutine (Dash_e (new Vector2 (DashPower, 0), 0.15f));
						else
							StartCoroutine (Dash_e (new Vector2 (-DashPower, 0), 0.15f));
					}
				}
			}

			if (playerActions.Special.IsPressed && playerActions.Special.LastState != playerActions.Special.IsPressed)
			{
				SpecialAttack (GetComponentInChildren<PlayerInfo>().playerName);
			}

			if (playerActions.Jump.IsPressed && playerActions.Jump.LastState != playerActions.Jump.IsPressed && jumpCount < jumpLimit)
			{
				anim.SetTrigger ("jump");
				rb2d.velocity = Vector2.zero;
				rb2d.angularVelocity = 0;
				rb2d.AddForce (new Vector2 (0f, jumpForce - (jumpCount * 1000f)));
				jumpCount++;
			}

			if (grounded && playerActions.Down.IsPressed) {
				anim.SetBool ("crouching", true);
				Crouching = true;
				rb2d.mass = 5;
			}
			else if (grounded && playerActions.Down.IsPressed &&  playerActions.Down.LastState != playerActions.Down.IsPressed|| Mathf.Abs(playerActions.Move) > 0)
			{
				anim.SetBool("crouching", false);
				Crouching = false;
				rb2d.mass = 5;
			}
		}
	}

	void OnCollisionEnter2D (Collision2D col)
	{
		if(col.gameObject.layer == LayerMask.NameToLayer ("Player"))
		{
			if (Dashing) {
				//Debug.Log ("me" + rb2d.velocity);
				//Debug.Log ("bef" + col.gameObject.GetComponent<ControlController> ().rb2d.velocity);

				if (col.gameObject.GetComponent<ControlController> ().Dashing == true) { //If both dashing
					//Both hit
					StartCoroutine(KnockBackOpponent_e(col.gameObject.GetComponent<ControlController> ().rb2d.velocity * 2, 0.2f));
					foreach (ContactPoint2D contact in col.contacts) {
						Instantiate (HitShockwave, new Vector2 (contact.point.x, contact.point.y), Quaternion.identity);
					}
				} 
				else if (opponent.GetComponent<ControlController>().Crouching == true)
				{
					
				}
				else 
				{
					//Hit them only
					//Debug.Log("hit");
					/*StartCoroutine(KnockBackOpponent_e(new Vector2(col.gameObject.GetComponent<ControlController> ().rb2d.velocity.x * 5, 
																	col.gameObject.GetComponent<ControlController> ().rb2d.velocity.y), KnockoutTime));*/
					//col.gameObject.GetComponent<ControlController>().
					if (SpecialAttacking) {
						StartCoroutine (KnockBackOpponent_e (col.gameObject.GetComponent<ControlController> ().rb2d.velocity * 4, 0.6f));
						foreach (ContactPoint2D contact in col.contacts) {
							Instantiate (HitShockwave, new Vector2 (contact.point.x, contact.point.y), Quaternion.identity);
						}
					} else {
						StartCoroutine (KnockBackOpponent_e (col.gameObject.GetComponent<ControlController> ().rb2d.velocity * 4, 0.2f));
						foreach (ContactPoint2D contact in col.contacts) {
							Instantiate (HitShockwave, new Vector2 (contact.point.x, contact.point.y), Quaternion.identity);
						}
						//Instantiate (HitShockwave, new Vector2 (transform.position.x, 6.5f), Quaternion.AngleAxis(180, Vector3.forward));
					}
				}
				//Debug.Log ("aft" + col.gameObject.GetComponent<ControlController> ().rb2d.velocity);

				Dashing = false;
				//Debug.Log ("hit");

			}
		}
	}

	public Transform topSprite;
	void Flip()
	{
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
		theScale = topSprite.localScale;
		theScale.x *= -1;
		topSprite.localScale = theScale;
	}

	IEnumerator Dash_e(Vector2 force, float time)
	{
		SoundManager.PlayMusic ("sfx_dash");
		Dashing = true;
		canDash = false;
		anim.SetBool ("dashing", true);

		StartCoroutine (DashCooldown_e (dashCooldown));

		rb2d.velocity = force;

		yield return new WaitForSeconds (time);
		rb2d.velocity = Vector2.zero;
		Dashing = false;
		anim.SetBool ("dashing", false);
		anim.SetBool ("slamming", false);
	}

	IEnumerator DashCooldown_e(float time)
	{
		yield return new WaitForSeconds (time);
		canDash = true;
	}

	IEnumerator Special_e(Vector2 force, float time)
	{
		Dashing = true;
		SpecialAttacking = true;
		canDash = false;
		canSpecial = false;
		anim.SetBool ("special", true);

		StartCoroutine (SpecialCooldown_e (specialCooldown));

		rb2d.velocity = force;

		yield return new WaitForSeconds (time);
		rb2d.velocity = Vector2.zero;
		canDash = true;
		Dashing = false;
		SpecialAttacking = false;
		anim.SetBool ("special", false);
	}

	IEnumerator SpecialCooldown_e(float time)
	{
		yield return new WaitForSeconds (time);
		canSpecial = true;
	}

	IEnumerator Roll_e(Vector2 force, float time)
	{
		Rolling = true;
		rb2d.velocity = force;

		yield return new WaitForSeconds (time);
		rb2d.velocity = Vector2.zero;
		Rolling = false;
		anim.SetBool ("rolling", false);
	}

	public IEnumerator KnockBackOpponent_e(Vector2 force, float time)
	{
		opponent.GetComponent<ControlController>().KnockedBack = true;
		SoundManager.PlayMusic ("sfx_hit");
		//Instantiate (HitShockwave, new Vector2 (transform.position.x, 6.5f), Quaternion.AngleAxis(180, Vector3.forward));

		yield return new WaitForSeconds (time);
		opponent.GetComponent<ControlController>().KnockedBack = false;
		//anim.SetBool ("knockout", false);
	}

	void SpecialAttack(string PlayerName)
	{
		switch (PlayerName)
		{
		case "Bull":
			if (canDash == true && canSpecial == true && !Crouching) {
				if (facingRight)
					StartCoroutine (Special_e (new Vector2 (DashPower * 0.5f, DashPower * 0.75f), 0.15f));
				else
					StartCoroutine (Special_e (new Vector2 (-DashPower * 0.5f, DashPower * 0.75f), 0.15f));
			}
			break;

		case "Grizzly":
			if (canDash == true && canSpecial == true && grounded == false && !Crouching) {
				if (facingRight)
					StartCoroutine (Special_e (new Vector2 (DashPower * 0.5f, -DashPower * 0.75f), 0.15f));
				else
					StartCoroutine (Special_e (new Vector2 (-DashPower * 0.5f, -DashPower * 0.75f), 0.15f));
			}
			break;

		case "Redbull":
			if (canDash == true && canSpecial == true && !Crouching) {
				//if (facingRight)
				//StartCoroutine (Special_e (new Vector2 (0, jumpForce), 0.5f));
				//else
					//StartCoroutine (Special_e (new Vector2 (0, DashPower * 1.5f), 0.15f));
				anim.SetBool ("special", true);
				rb2d.velocity = Vector2.zero;
				rb2d.angularVelocity = 0;
				rb2d.AddForce (new Vector2 (0f, jumpForce));
				Dashing = true;
				//jumpCount++;
			}
			break;

		case "Polar":
			if (canDash == true && canSpecial == true && !Crouching) {
				if (facingRight)
					StartCoroutine (Special_e (new Vector2 (DashPower * 0.3f, 0), 0.6f));
				else
					StartCoroutine (Special_e (new Vector2 (-DashPower * 0.3f, 0), 0.6f));
			}
			break;
		}
	}

	/*
	IEnumerator Special_e(Vector2 force, float time)
	{
		Dashing = true;
		canDash = false;
		anim.SetBool ("dashing", true);

		StartCoroutine (DashCooldown_e (dashCooldown));

		rb2d.velocity = force;

		yield return new WaitForSeconds (time);
		rb2d.velocity = Vector2.zero;
		Dashing = false;
		anim.SetBool ("dashing", false);
		anim.SetBool ("slamming", false);
	}
	*/
}
