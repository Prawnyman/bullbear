﻿using UnityEngine;
using System.Collections;

public class DeathManager : MonoBehaviour {

	GameObject manager;
	GameObject player;

	public float leftDeath	= -22f;
	public float rightDeath = 22f;
	public float downDeath 	= -15f;
	public float upDeath 	= 18f;

	public Transform explosion;
	public bool alive = true;

	// Use this for initialization
	void Start () {
		manager = GameObject.Find ("GameManager");
		player = this.gameObject;
	}

	// Update is called once per frame
	void Update () {
		if (alive) {
			if (player.transform.position.x < leftDeath) {
				alive = false;
				Death ();
				Instantiate (explosion, new Vector2 (-14, player.transform.position.y), Quaternion.AngleAxis(-90, Vector3.forward));	
			} else if (player.transform.position.x > rightDeath) {
				alive = false;
				Death ();
				Instantiate (explosion, new Vector2 (14, player.transform.position.y), Quaternion.AngleAxis(90, Vector3.forward));
			} else if (player.transform.position.y < downDeath) {
				alive = false;
				Death ();
				Instantiate (explosion, new Vector2 (player.transform.position.x, -6.5f), Quaternion.identity);	
			} else if (player.transform.position.y > upDeath) {
				alive = false;
				Death ();
				Instantiate (explosion, new Vector2 (player.transform.position.x, 6.5f), Quaternion.AngleAxis(180, Vector3.forward));	
			}
		}
	}

	void Death()
	{
		//Destroy (player);
		//player.SetActive(false);
		if (player.GetComponent<PlayerInfo> ().playerNumber == 1)
			manager.GetComponent<GameManager> ().p1Lives--;
		else
			manager.GetComponent<GameManager> ().p2Lives--;
		SoundManager.PlayMusic ("sfx_death");
		StartCoroutine (Respawn (1.5f));
	}

	IEnumerator Respawn(float time)
	{
		yield return new WaitForSeconds (time);
		alive = true;
		player.GetComponent<ControlController> ().rb2d.velocity = Vector2.zero;
		player.transform.position = new Vector2(Random.Range(-8, 8), 13);
	}
}
