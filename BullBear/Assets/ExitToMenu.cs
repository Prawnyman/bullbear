﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class ExitToMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown("escape"))
			SceneManager.LoadScene("Main Menu");
	}
}
